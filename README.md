# Personal Projects

## [LIBMIN](https://micha.monoid.net/libmin)

Minimal C libraries (single header):

- delaunay triangulation
- binary heap data-structure
- ring buffer data-structure
- token / line parser (with comment support)
<!-- -geometric structures and function -->

## [VKU](https://micha.monoid.net/vku)

[Vulkan](https://www.vulkan.org) utility functions:

- result names and debug logger
- extension and layer filtering
- color image transfer and mip-level generation

## [Mosaic](https://gitlab.com/monoid-labs/mosaic)

File format collection optimized for streaming assets:

- Triangle meshes with skinning and morph-target support.
- Transform hierarchies with animations support.
- Examples: [Blender](https://www.blender.org) exporter, [Unity](https://unity.com) importer, ..

NOTE: legacy version; improved formats and tools will be released soon^tm.

## [Unity Packages](https://gitlab.com/monoid-labs/unity-packages)

[Unity](https://unity.com) packages for:

- animation: 1D & 2D blend-graphs and state-machine `Playable`s.
- collections: native ring-buffer (aka circular-buffer)
- mathematics: more math functions / structures
- graphics: fast mesh generation for simple shapes, gradient texture assets, ..
- assets: helpers for writing asset tools like scripted importers.

## [Go Chat Server](https://gitlab.com/monoid-labs/go-modules/chat)

[GO](https://go.dev/) Chat server for session based games.

- Optimized for concurrency with low cpu and memory costs
- No message history (after re-connect or restart)
